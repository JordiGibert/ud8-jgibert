import dto.Persona;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Persona per1=new Persona();
		Persona per2=new Persona("Jordi", 21, 'H');
		Persona per3=new Persona("Maria", 32, "21705127G", 'M', 1.67, 67.2);
		
		System.out.println(per1.toString());
		System.out.println(per2.toString());
		System.out.println(per3.toString());
		
	}

}
