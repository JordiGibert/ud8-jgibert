package dto;

public class Persona {
	final String defaultS= "";
	final int defaultI = 0;
	final double defaultD= 0.0;
	final char defaultSexo= 'H';
	
	private String nombre;
	private int edad;
	private String DNI;
	private char sexo;
	private double peso;
	private double altura;
	
	public Persona() {
		this.nombre = defaultS;
		this.edad = defaultI;
		DNI = "2154875G";
		this.sexo = defaultSexo;
		this.peso = defaultD;
		this.altura = defaultD;
	}

	public Persona(String nombre, int edad, char sexo) {
		this.nombre = nombre;
		this.edad = edad;
		DNI = "2154875G";
		this.sexo = sexo;
		this.peso = defaultD;
		this.altura = defaultD;
	}

	public Persona(String nombre, int edad, String dNI, char sexo, double peso, double altura) {
		this.nombre = nombre;
		this.edad = edad;
		DNI = dNI;
		this.sexo = sexo;
		this.peso = peso;
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", DNI=" + DNI + ", sexo=" + sexo + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}
	
	
	
	
	
	
	
}
