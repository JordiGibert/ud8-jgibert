import dto.Serie;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Serie serie1=new Serie();
		Serie serie2=new Serie("Pocoyo","Pere Marti");
		Serie serie3=new Serie("Els barrofets", 12, "infantil", "Juan Garcia");
		
		System.out.println(serie1.toString());
		System.out.println(serie2.toString());
		System.out.println(serie3.toString());
	}

}
