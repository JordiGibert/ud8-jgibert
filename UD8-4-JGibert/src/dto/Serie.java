package dto;

public class Serie {
	final int DnumTemp=3;
	final boolean Dentregado=false;
	final String DString = "";
	
		
	private String titulo;
	private int numTemp;
	private boolean entregado;
	private String genero;
	private String creador;
	
	public Serie() {
		this.titulo = DString;
		this.numTemp = DnumTemp;
		this.entregado = Dentregado;
		this.genero = DString;
		this.creador = DString;
	}

	public Serie(String titulo, String creador) {
		
		this.titulo = titulo;
		this.numTemp = DnumTemp;
		this.entregado = Dentregado;
		this.genero = DString;
		this.creador = creador;
	}

	public Serie(String titulo, int numTemp, String genero, String creador) {
		this.titulo = titulo;
		this.numTemp = numTemp;
		this.entregado = Dentregado;
		this.genero = genero;
		this.creador = creador;
	}

	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numTemp=" + numTemp + ", entregado=" + entregado + ", genero=" + genero
				+ ", creador=" + creador + "]";
	}
	
	
	
	
}
