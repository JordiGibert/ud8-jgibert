package dto;

import java.util.Random;



public class Password {
	
	final int DefaultLongitud = 8;

	private String contrasenya;
	
	private int longitud; 
	
	
	
	public Password(int longitud) {
		this.longitud = longitud;
		contrasenya=crearContra(longitud);
	}
	
	public Password() {
		this.longitud = DefaultLongitud;
		contrasenya=crearContra(longitud);
	}



	@Override
	public String toString() {
		return "Password [contrasenya=" + contrasenya + ", longitud=" + longitud + "]";
	}

	private static String crearContra(int longitud) {
		char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJQLMNOPQRSTUVWXYZ0123456789".toCharArray();
		StringBuilder sb = new StringBuilder(longitud);
		Random random = new Random();
		for (int i = 0; i < longitud; i++) {
		       char c = chars[random.nextInt(chars.length)];
		       sb.append(c);
		}
		String output = sb.toString();
		return output;
	}
}
