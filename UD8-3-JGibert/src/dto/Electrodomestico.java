package dto;

public class Electrodomestico {
	
	final char DConsumo = 'F';
	final double DPrecio= 100.0;
	final String DColor = "Blanco";
	final double DPeso = 5.0;
	
	private double precioBase;
	private String color;
	private char consumo;
	private double peso;
	
	public Electrodomestico() {
		this.precioBase = DPrecio;
		this.color = DColor;
		this.consumo = DConsumo;
		this.peso = DPeso;
	}
	
	
	public Electrodomestico(double precioBase, double peso) {

		this.precioBase = precioBase;
		this.color = DColor;
		this.consumo = DConsumo;
		this.peso = peso;
	}


	public Electrodomestico(double precioBase, String color, char consumo, double peso) {
		super();
		this.precioBase = precioBase;
		if(color.equalsIgnoreCase("blanco") || color.equalsIgnoreCase("negro") || color.equalsIgnoreCase("azul") || color.equalsIgnoreCase("rojo") || color.equalsIgnoreCase("gris")) {
			this.color = color;
		} else {
			this.color = DColor;
		}
		if(consumo=='A'||consumo=='B'||consumo=='C'||consumo=='D'||consumo=='E'||consumo=='F') {
			this.consumo = consumo;
		} else {
			this.consumo = DConsumo;
		}
		this.peso = peso;
	}


	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumo=" + consumo + ", peso="
				+ peso + "]";
	}
	
	
	
	

}
